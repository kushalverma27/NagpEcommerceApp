import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-welcome-page",
  styleUrls: ["./welcome-page.component.css"],
  templateUrl: "./welcome-page.component.html"
})
export class WelcomePageComponent implements OnInit {

  constructor() {
    // Empty.
  }

  public ngOnInit(): void {
   // Empty
  }

}
